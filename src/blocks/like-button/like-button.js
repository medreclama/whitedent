const likeButton = () => {
  const buttons = Array.from(document.querySelectorAll('.like-button__button > a'));
  buttons.forEach((button) => {
    button.addEventListener('click', (e) => e.preventDefault());
  });
};

export default likeButton;
