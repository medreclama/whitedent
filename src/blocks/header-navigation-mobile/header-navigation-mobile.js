import parentHasClass from '../../scripts/modules/parentHasClass';

const headerNavigationMobile = () => {
  const addSwitcher = (elem, className) => {
    const switcher = document.createElement('div');
    switcher.classList.add(`${className}__switcher`);
    switcher.addEventListener('click', (e) => e.target.parentElement.classList.toggle(`${className}__item--open`));
    elem.after(switcher);
  };

  const windowNoScroll = (on = true) => {
    const htmlElement = document.querySelector('html');
    if (on) htmlElement.style.setProperty('overflow', 'hidden');
    else htmlElement.style.removeProperty('overflow');
  };

  const navMob = document.querySelector('.header-navigation-mobile');
  if (navMob) {
    const button = navMob.querySelector('.header-navigation-mobile__button');

    button.addEventListener('click', () => {
      navMob.classList.toggle('header-navigation-mobile--active');
      if (navMob.classList.contains('header-navigation-mobile--active')) windowNoScroll();
      else windowNoScroll(false);
      navMob.style.setProperty('--body-height', `${document.documentElement.clientHeight - button.getBoundingClientRect().bottom}px`);
    });
    window.addEventListener('click', (e) => {
      if (!parentHasClass(e.target, 'header-navigation-mobile') && navMob.classList.contains('header-navigation-mobile--active')) {
        navMob.classList.remove('header-navigation-mobile--active');
        windowNoScroll(false);
      }
    });
    const linksServices = Array.from(navMob.querySelectorAll('.menu-services__item--parent > a'));
    const linksMain = Array.from(navMob.querySelectorAll('.header-navigation__item--parent > a'));
    if (linksServices) linksServices.forEach((a) => addSwitcher(a, 'menu-services'));
    if (linksMain) linksMain.forEach((a) => addSwitcher(a, 'header-navigation'));
  }

  const navigation = document.querySelector('.header__navigation');
  const html = document.querySelector('html');
  const fixPoint = Math.round(navigation.getBoundingClientRect().top + window.pageYOffset);
  const mobNav = navigation.querySelector('.header__mobile-navigation');
  const menuClasses = 'grid__cell grid__cell--hd-hidden grid__cell--xl-hidden grid__cell--l-hidden grid__cell--m-6 grid__cell--s-6 grid__cell--xs-5 header__mobile-navigation';
  const menuClassesFixed = 'grid__cell grid__cell--hd-hidden grid__cell--xl-hidden grid__cell--l-hidden grid__cell--m-2 grid__cell--s-2 grid__cell--xs-2 header__mobile-navigation';
  window.addEventListener('scroll', () => {
    if (window.pageYOffset >= fixPoint && !navigation.classList.contains('header__navigation--fixed')) {
      navigation.classList.add('header__navigation--fixed');
      mobNav.className = menuClassesFixed;
      html.style.setProperty('--margin-top-mobile', `${navigation.offsetHeight}px`);
    } else if (window.pageYOffset < fixPoint) {
      navigation.classList.remove('header__navigation--fixed');
      mobNav.className = menuClasses;
      html.style.removeProperty('--margin-top-mobile');
    }
  });
};

export default headerNavigationMobile;
