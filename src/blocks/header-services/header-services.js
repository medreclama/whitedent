import Menu from '../menu/menu';
import menuServices from '../menu-services/menu-services';

const headerService = () => {
  const element = document.querySelector('.js-header-services');
  const response = fetch('/menu.php?type=services', {
    method: 'GET',
  });
  response.then((result) => result.text())
    .then((result) => {
      const regex = /,(?=\s*?[}\]])/g;
      return result.replace(regex, '');
    })
    .then((result) => {
      const menu = JSON.parse(result);
      element.append(new Menu(menu, 'menu-services').menu());
    })
    .then(() => {
      const servicesButton = document.querySelector('.header-services__button');
      if (servicesButton) {
        servicesButton.addEventListener('click', (e) => e.currentTarget.parentElement.classList.toggle('header-services--active'));
      }
      menuServices();
      document.dispatchEvent(new CustomEvent('servicesLoaded'));
    });
};

export default headerService;
