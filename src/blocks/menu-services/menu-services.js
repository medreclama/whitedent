import parentHasClass from '../../scripts/modules/parentHasClass';

const menuServices = () => {
  const ms = Array.from(document.querySelectorAll('.menu-services'));
  ms.forEach((menu) => {
    if (!parentHasClass(menu, 'header-navigation-mobile')) {
      const setSizes = () => new Promise((res) => {
        // Добавляем класс, с которым все размеры посчитаются правильно.
        // Он отлючает трансформацию контейнера, но оставляет его невидимым для пользователя
        menu.classList.add('menu-services--loading');
        res(true);
      })
        .then(() => {
          // Считаем ширину контейнера из родителя с классом .content и списка первого уровня
          const parentContent = parentHasClass(menu, 'content', true);
          const parentContentWidth = parentContent.offsetWidth
            // Удаляем поля из ширины контейнера
            - parseInt(getComputedStyle(parentContent).getPropertyValue('padding-left'), 10)
            - parseInt(getComputedStyle(parentContent).getPropertyValue('padding-right'), 10);

          // Устанавливаем ширину
          menu.style.setProperty('--sub-width', `${parentContentWidth - menu.offsetWidth}px`);

          menu.style.setProperty('--sub-min-height', `${menu.offsetHeight}px`);

          // Считаем высоту контейнера
          const subLists = Array.from(menu.querySelectorAll('.menu-services__sublist--1'));
          subLists.forEach((item) => {
            // Массив со всеми высотами элементов:
            const heights = Array.from(item.children).map((child) => child.offsetHeight);

            // Средняя высота элементов:
            const avgHeight = heights.reduce((a, b) => a + b, 0) / item.children.length;

            // Максимальная из высот:
            const maxHeight = Math.max(...heights);

            // Для высоты контейнера выбираем максимальное значение из средней и максимальной высот.
            // Это нужно для случая, когда максимальная высота больше, чем средняя.
            let h = Math.max(avgHeight, maxHeight);

            // Устанавливаем высоту в css-свойство
            item.style.setProperty('--sub-height', `${h}px`);

            // Проверка, все ли элементы помещаются по ширине.
            // Если нет, то к высоте контейнера добавляется высота не помещающегося элемента
            Array.from(item.children).forEach((c) => {
              while (c.getBoundingClientRect().right > item.getBoundingClientRect().right) {
                h += 8;
                item.style.setProperty('--sub-height', `${h}px`);
              }
            });
          });
        })
        .then(() => {
          // Удаляем вспомогательный класс
          menu.classList.remove('menu-services--loading');
        });
      window.addEventListener('resize', () => requestAnimationFrame(setSizes));
      setSizes();
    }
  });
};

export default menuServices;
