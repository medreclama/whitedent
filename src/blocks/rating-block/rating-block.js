const ratingBlock = () => {
  const blocks = Array.from(document.querySelectorAll('.rating-block'));
  blocks.forEach((block) => {
    const starsList = block.querySelector('.rating-block__stars');
    const stars = Array.from(block.querySelectorAll('.rating-block__star'));
    const starsCount = +block.dataset.starsCount || stars.length;
    const rating = +block.dataset.rating;
    if (!rating) return;
    if (block.dataset.starsCount) {
      if (stars.length < starsCount) {
        for (let i = 0; i < starsCount - stars.length; i += 1) starsList.innerHTML += '<div class="rating-block__star"></div>';
      }
      if (stars.length > starsCount) {
        stars.forEach((star, i) => {
          if (i >= stars.length - starsCount) star.remove();
        });
      }
    }
    if (rating < starsCount) {
      stars.forEach((star, i) => {
        if (i < Math.floor(rating)) star.style.setProperty('--gradient-stop', '100%');
        else if (i === Math.floor(rating)) star.style.setProperty('--gradient-stop', `${(rating - Math.floor(rating)) * 100}%`);
      });
    } else {
      stars.forEach((star) => star.style.setProperty('--gradient-stop', '100%'));
    }
  });
};

export default ratingBlock;
