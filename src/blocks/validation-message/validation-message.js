const validationMessage = () => {
  const messages = document.querySelectorAll('.validation-message');
  const inputs = document.getElementsByTagName('input');

  if (!messages) return;

  const disableMessages = () => {
    Array.from(messages).forEach((m) => {
      const msg = m;
      if (msg.classList.contains('validation-message--active')) {
        msg.classList.remove('validation-message--active');
        setTimeout(() => {
          msg.style.display = '';
        }, 200);
      }
    });
  };

  window.addEventListener('click', disableMessages);
  Array.from(inputs).forEach((input) => input.addEventListener('focus', disableMessages));
};

export default validationMessage;
