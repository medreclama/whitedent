const reviewFormRating = () => {
  const block = document.querySelector('.review-form-rating');
  if (!block) return;
  const list = block.querySelector('.review-form-rating__list');
  const stars = Array.from(block.querySelectorAll('.review-form-rating__star'));
  const input = block.querySelector('.form-input__field');
  let rating = 0;
  input.value = rating;
  list.addEventListener('click', (e) => {
    if (!e.target.classList.contains('review-form-rating__star')) return;
    rating = e.target.dataset.rating;
    input.value = rating;
    stars.forEach((star, i) => {
      if (i < rating) star.style.setProperty('--gradient-stop', '100%');
      else star.style.setProperty('--gradient-stop', '0');
    });
  });
};

export default reviewFormRating;
