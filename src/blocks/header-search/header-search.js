const headerSearch = () => {
  const hs = document.querySelector('.header-search:not(.header-search--home-page)');
  if (hs) {
    const form = hs.querySelector('.header-search__form');
    const parent = hs.parentElement.parentElement;
    const open = hs.querySelector('.header-search__open');
    const close = hs.querySelector('.header-search__close');
    const width = hs.getBoundingClientRect().right - parent.getBoundingClientRect().left;
    form.style.setProperty('width', `${width - 16}px`);
    if (open) open.addEventListener('click', () => hs.classList.add('header-search--active'));
    if (close) {
      close.addEventListener('click', (e) => {
        e.preventDefault();
        hs.classList.remove('header-search--active');
      });
    }
  }
};

export default headerSearch;
