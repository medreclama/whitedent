export default function button() {
  const rad = (item, value) => item.style.setProperty('--btn-gradient-radius', `${value}px`);

  const setCenter = (e) => {
    const xPos = e.x - e.target.getBoundingClientRect().left;
    const yPos = e.y - e.target.getBoundingClientRect().top;
    e.target.style.setProperty('--btn-gradient-center-x', `${xPos}px`);
    e.target.style.setProperty('--btn-gradient-center-y', `${yPos}px`);
  };

  const getRadius = (item) => {
    const sqr = item.offsetWidth * item.offsetWidth + item.offsetHeight * item.offsetHeight;
    return Math.sqrt(sqr);
  };

  const btns = Array.from(document.querySelectorAll('.button'));

  btns.forEach((btn) => {
    const radius = getRadius(btn);
    btn.addEventListener('mouseover', (e) => {
      setCenter(e);
      const step = radius / (60 * 0.2);
      let val = 0;
      const tick = () => {
        const raf = requestAnimationFrame(tick);
        if (val <= radius) {
          rad(e.target, val);
          val += step;
        }
        if (val > radius) {
          rad(e.target, radius);
          cancelAnimationFrame(raf);
        }
      };
      tick();
    });
    btn.addEventListener('mouseleave', (e) => {
      setCenter(e);
      const step = -(radius / (60 * 0.2));
      let val = radius;
      const tick = () => {
        const raf = requestAnimationFrame(tick);
        if (val >= 0) {
          rad(e.target, val);
          val += step;
        }
        if (val < 0) {
          rad(e.target, 0);
          cancelAnimationFrame(raf);
        }
      };
      tick();
    });
  });
}
