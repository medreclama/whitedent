import Menu from '../menu/menu';

const headerNavigation = () => {
  const response = fetch('/menu.php?type=navigation', {
    method: 'GET',
  });
  response.then((result) => result.text())
    .then((result) => {
      const regex = /,(?=\s*?[}\]])/g;
      return result.replace(regex, '');
    })
    .then((result) => {
      const menu = JSON.parse(result);
      const element = document.querySelector('.js-header-menu');
      element.append(new Menu(menu, 'header-navigation').menu());
    })
    .then(() => {
      const parents = Array.from(document.querySelectorAll('.header-navigation__item--parent'));
      parents.forEach((parent) => parent.style.setProperty('--arrow-position', `${parent.offsetWidth - 12}px`));
    });
};

export default headerNavigation;
