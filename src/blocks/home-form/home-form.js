const homeForm = () => {
  const hf = document.querySelector('.home-form');
  if (hf) {
    const image = hf.querySelector('.home-form__image');
    const img = image.querySelector('img');
    const setSize = () => img.style.setProperty('width', `${image.getBoundingClientRect().left + image.offsetWidth}px`);
    setSize();
    window.addEventListener('resize', () => requestAnimationFrame(setSize));
  }
};

export default homeForm;
