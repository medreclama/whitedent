const discountForm = () => {
  const block = document.getElementById('discount-form');
  const form = document.getElementById('discount-form-form');
  const buttons = document.querySelectorAll('.discount-form-open');
  const types = {
    doctor: 'Хочу записаться к врачу',
    discount: 'Хочу записаться на акцию',
  };

  if (!buttons || !block) return;

  Array.from(buttons).forEach((btn) => {
    btn.addEventListener('click', (e) => {
      const { type, from, elementid } = e.target.dataset;
      form.from.value = from;
      if (type && from) form.message.value = `${types[type]} "${from}"`;
      if (type) form.page_type.value = type;
      if (elementid) form.element_id.value = elementid;
      block.style.display = 'flex';
      setTimeout(() => {
        block.classList.add('discount-form--active');
      }, 20);
    });
  });

  block.addEventListener('click', (e) => {
    const isBlock = e.target.classList.contains('discount-form');
    const isClose = e.target.closest('.discount-form__close');

    if (isBlock || isClose) {
      block.classList.remove('discount-form--active');
      form.from.value = '';
      form.message.value = '';
      form.page_type.value = '';
      form.element_id.value = '';
      setTimeout(() => {
        block.style.display = '';
      }, 200);
    }
  });
};

export default discountForm;
