class Menu {
  #list;
  #className;
  #level = 0;

  constructor(list, className) {
    this.#list = list;
    this.#className = className;
  }

  #menuItem = (item) => {
    const itemClass = this.#className.split(' ').map((classItem) => `${classItem}__item`);
    if (item.active) itemClass.push(...this.#className.split(' ').map((classItem) => `${classItem}__item--active`));
    if (item.sublist) itemClass.push(...this.#className.split(' ').map((classItem) => `${classItem}__item--parent`));
    if (this.#level > 0) itemClass.push(...this.#className.split(' ').map((classItem) => `${classItem}__item--sub ${classItem}__item--sub-${this.#level}`));
    const res = document.createElement('li');
    res.className = itemClass.join(' ');
    res.insertAdjacentHTML('beforeend', ` <a href="${item.href}">${item.name}</a>`);
    if (item.sublist) {
      this.#level += 1;
      res.append(this.#menuList(item.sublist));
      this.#level -= 1;
    }
    return res;
  };

  #menuList = (list) => {
    const listClass = this.#className.split(' ').map((item) => {
      if (this.#level > 0) return `${item}__sublist ${item}__sublist--${this.#level}`;
      return `${item}__list`;
    }).join(' ');
    const res = document.createElement('ul');
    res.className = listClass;
    list.forEach((item) => res.append(this.#menuItem(item)));
    return res;
  };

  menu = () => {
    const res = document.createElement('nav');
    res.className = this.#className;
    res.append(this.#menuList(this.#list));
    return res;
  };
}

export default Menu;
