import { Splide } from '@splidejs/splide';
import likely from 'ilyabirman-likely';
import lightbox from './modules/lightbox';
import sh from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import button from '../blocks/button/button';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import headerNavigationMobile from '../blocks/header-navigation-mobile/header-navigation-mobile';
import headerSearch from '../blocks/header-search/header-search';
import headerService from '../blocks/header-services/header-services';
import homeForm from '../blocks/home-form/home-form';
import ratingBlock from '../blocks/rating-block/rating-block';
import reviewItem from '../blocks/review-item/review-item';
import countdown from '../blocks/countdown/countdown';
import likeButton from '../blocks/like-button/like-button';
import reviewFormRating from '../blocks/review-form-rating/review-form-rating';
import discountForm from '../blocks/discount-form/discount-form';
import validationMessage from '../blocks/validation-message/validation-message';
import counters from './modules/counters';
import './modules/popup';

const sliders = Array.from(document.querySelectorAll('.splide'));

// TODO: отключить управление с клавиатуры для слайдера

sliders.forEach((slider) => {
  if (slider.classList.contains('slider-doctors')) {
    new Splide('.slider-doctors', {
      type: 'loop',
      gap: '2rem',
      perPage: 2,
      arrows: { prev: HTMLButtonElement, next: HTMLButtonElement },
      breakpoints: {
        1000: {
          perPage: 1,
        },
      },
    }).mount();
  } else {
    new Splide(slider, {
      type: 'loop',
      gap: '2rem',
      arrows: { prev: HTMLButtonElement, next: HTMLButtonElement },
    }).mount();
  }
});

lightbox();
sh();
tabs();
button();
headerNavigation();
headerNavigationMobile();
headerSearch();
headerService();
homeForm();
ratingBlock();
reviewItem();
countdown();
likeButton();
reviewFormRating();
likely.initiate();
discountForm();
validationMessage();

const countersCodeHead = `
<script data-skip-moving="true" type="text/javascript">
  var __cs = __cs || [];
  __cs.push(["setCsAccount", "fcGggTfMftnyzGZs41LCv3_Nm5x_Ri6T"]);
</script>
`;
const countersCodeBody = `
  <!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(25626353, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25626353" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112711156-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-112711156-1');
    setTimeout(function(){
        ga('send', 'event', 'New Visitor', location.pathname);
    }, 15000);
</script>
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);
