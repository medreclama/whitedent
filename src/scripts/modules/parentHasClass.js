const check = (child, classname, returnElement) => {
  if (child.classList.contains(classname)) return returnElement !== true ? true : child;
  try {
    return child.parentNode && check(child.parentNode, classname, returnElement);
  } catch (TypeError) {
    return false;
  }
};

export default check;
