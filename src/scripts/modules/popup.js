function onBlockClick(e, block) {
  const isBlock = e.target.classList.contains('discount-form');
  const isClose = e.target.closest('.discount-form__close');

  if (isBlock || isClose) {
    // eslint-disable-next-line no-param-reassign
    block.classList.remove('discount-form--active');
    setTimeout(() => {
      // eslint-disable-next-line no-param-reassign
      block.style.display = '';
    }, 200);
  }
}

const buttons = document.querySelectorAll('.btn-popup');

Array.from(buttons).forEach((btn) => {
  btn.addEventListener('click', (e) => {
    e.preventDefault();
    const block = document.querySelector(e.target.getAttribute('href'));
    if (block) {
      block.style.display = 'flex';
      setTimeout(() => {
        block.classList.add('discount-form--active');
      }, 20);
      block.addEventListener('click', (event) => onBlockClick(event, block));
    }
  });
});
